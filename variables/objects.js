//Objects : key value pairs

let cuboid = {
    height : 5,
    width  : 2,
    length : 4,
    weight : "40 kgs",
    isRed  : false,
    volume(){
        console.log(this.height * this.width * this.length)
    }
}
// console.log(`My cuboid weighs : ${cuboid.weight}.`)

//DELETE a property
    delete cuboid.isRed
    console.log(cuboid) //{ height: 5, width: 2, length: 4, weight: '40 kgs' }

//ADD a property
    cuboid['lable'] = "Parcel"
    console.log(cuboid.lable)


/** using functions witin objects */
    console.log(cuboid.volume())



/********Use in function format***********/
    function person(name,age){
        return{
            name : name,
            age : age
        }
    }

    let student = person('Peter',16)
    console.log(`${student.name} is ${student.age} years old.`)


