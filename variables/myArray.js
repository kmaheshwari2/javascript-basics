let colors = ["orange" , "red" , "green" , {fav : "blue"} , 400 , 500]
console.log(colors[2]) //green

/**Replace elements */
    colors[1] = "pink"
    console.log(colors[1])



/**Length of array */
console.log(colors.length)

console.log(colors[3].fav)


/**POPPING form an array */
colors.pop()
console.log(colors)

/**PUSHING into an array */
colors.push("white")
console.log(colors)

/** shift() */
colors.shift()
console.log(colors)


/**unshift() */
colors.unshift("black")
console.log(colors)


for(let clrs of colors){
    console.log(clrs)
}