//Js datatypes : used to hold data.

/********** NOTE ************
 * Js is Loosely typed : no type decleartion required
 * Js id Dynamically typed : type is alloted dynamically.
*/

//Primitive datatypes : Number, staring, boolean, null, undefined

//Number
    var a = 10   //integer values allowed
    var b = 2.1  //Floating values allowed

    // console.log(`${a} ${b}`)



//String
    var fName = "Krati"
    var lName = 'Maheshwari'
    var designation = `Solution Engineer`

    // console.log(`My name is ${fName} ${lName}. I am a ${designation} at Deqode.`)



//Boolean :true/false
    var compare  = 6 < 1
    // console.log(compare) //false
    // console.log(!compare)//True



//null : nothing or empty calue
    var height = null //ie, height is unknown



//undefined : it means the value is not assigned
    var marks 
    // console.log(marks) //undefined
    // console.log(typeof marks)


