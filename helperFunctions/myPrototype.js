function Mobile(brand,ram,color){
    this.brand = brand,
    this.ram = ram,
    this.color = color
}
//Using Prototypefor properties
Mobile.prototype.memory = "64GB"

//Using prototype for functions
Mobile.prototype.features = function(){
    console.log(`${this.brand} of ${this.ram} in ${this.color} with ${this.memory} is awailable.`)
} 

var myPhone = new Mobile("iPhone" , 4 , "Black")

myPhone.features(); //iPhone of 4 in Black with 64GB is awailable.