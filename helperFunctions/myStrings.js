//Strings and its length
var car1 = "Hundai"
var car2 = 'Pagani'
console.log(car2.length)

//Escape char within strings
var escapeChar = 'this is Huma\'s book'
console.log(escapeChar)


//String and objects
var name = "Peter"             //String
var Name = new String("Peter") //Object
console.log(typeof name , typeof Name)
console.log(name == Name)
console.log(name === Name)

//indexOf
var pos = escapeChar.indexOf("is")
var pos1 = escapeChar.lastIndexOf("is")

console.log(pos)
console.log(pos1)

