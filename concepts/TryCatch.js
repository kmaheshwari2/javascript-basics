const convKmToMeter = (kiloMeter) => {
    if(typeof kiloMeter === 'number'){
        return kiloMeter * 1000
    } else {
        throw Error("Please enter a valid number")
    }
}

try{
    const myValue = convKmToMeter(3)
    console.log(myValue)
}catch(e){
    console.log(e)
}


console.log("This is last part of file")