//method -> object

const book = {
    title : "abc",
    color(){
        console.log(this) //here this refers to book object
    }
}

book.pages = function(){
    console.log(this)
}

book.pages();


//Function -> global

