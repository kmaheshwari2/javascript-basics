//Function decleration


sum(2,4) //can use before decleration

function sum(a,b){
    return console.log(a+b)
}


//Function expression

let square = function(x){
    let result = x*x;
    return console.log(result);
};

square(4)

