//Functions with local variables:

function greetPeople(){
    let msg = "This is a sunny day"
    console.log(msg)
}

// greetPeople();


let Name = "mike"

function showUserName(){
    Name = "bob"
    console.log ("name is :" +Name)
} 

console.log(Name) //mike

showUserName() //bob

console.log(Name) //bob

/**
 * if the same variables are declared inside the function, they outshadow the outer one
 */

let Name = "mike"

function showUserName(){
    let Name = "bob"
    console.log ("name is :" +Name)
} 

console.log(Name) //mike

showUserName() //mike

