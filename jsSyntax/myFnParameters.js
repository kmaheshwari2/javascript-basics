function add(a,b){
    return console.log( a+b );
}

add(3,5) //parameters 3,5

//To set default values:
function population(country, people = " not provided"){
    console.log(`${country} : ${people}`)
}

population("india")
population("japan","2000K")

function isEligible(age){
    if(age>18){
        console.log("You can vote!")

    }else {
        console.log("You can't vote!")
    }
}

isEligible(55)



